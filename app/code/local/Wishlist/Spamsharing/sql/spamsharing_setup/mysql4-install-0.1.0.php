<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('spamsharing')};
CREATE TABLE {$this->getTable('spamsharing')} (
  `spamsharing_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` varchar(255) NOT NULL default '',
  `sent_emails` varchar(255) NOT NULL default '',
  `update_time` date NULL,
  PRIMARY KEY (`spamsharing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 