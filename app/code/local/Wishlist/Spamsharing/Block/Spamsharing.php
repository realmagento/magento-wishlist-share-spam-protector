<?php
class Wishlist_Spamsharing_Block_Spamsharing extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getSpamsharing()     
     { 
        if (!$this->hasData('spamsharing')) {
            $this->setData('spamsharing', Mage::registry('spamsharing'));
        }
        return $this->getData('spamsharing');
        
    }
}