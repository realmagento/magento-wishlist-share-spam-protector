<?php
class Wishlist_Spamsharing_Model_Cron {

    public function removeAllSpamsharing() {
        $shareModel = Mage::getModel('spamsharing/spamsharing')->getCollection();
        foreach ($shareModel as $model) {
        	$model->delete();
        }
    }
}