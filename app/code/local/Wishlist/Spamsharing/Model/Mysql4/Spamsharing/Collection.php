<?php

class Wishlist_Spamsharing_Model_Mysql4_Spamsharing_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('spamsharing/spamsharing');
    }
}