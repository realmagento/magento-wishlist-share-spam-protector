<?php

class Wishlist_Spamsharing_Model_Mysql4_Spamsharing extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the spamsharing_id refers to the key field in your database table.
        $this->_init('spamsharing/spamsharing', 'spamsharing_id');
    }
}