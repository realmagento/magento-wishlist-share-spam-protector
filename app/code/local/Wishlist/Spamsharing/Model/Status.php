<?php

class Wishlist_Spamsharing_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('spamsharing')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('spamsharing')->__('Disabled')
        );
    }
}