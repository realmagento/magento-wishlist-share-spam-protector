<?php

class Wishlist_Spamsharing_Model_Spamsharing extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('spamsharing/spamsharing');
    }
}