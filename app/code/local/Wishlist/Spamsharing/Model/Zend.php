<?php
class Wishlist_Spamsharing_Model_Zend extends Mage_Captcha_Model_Zend
{
    protected function _isUserAuth()
    {
        if($this->_formId == 'wishlist')
        {
            return false;
        }
        return Mage::app()->getStore()->isAdmin()
            ? Mage::getSingleton('admin/session')->isLoggedIn()
            : Mage::getSingleton('customer/session')->isLoggedIn();
    }
}
